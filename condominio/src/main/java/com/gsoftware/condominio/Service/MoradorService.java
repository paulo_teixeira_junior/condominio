package com.gsoftware.condominio.Service;

import java.util.List;

import com.gsoftware.condominio.model.Morador;

public interface MoradorService {

	public List<Morador> getAll();
	
	public Morador getMorador(Long id);
	
	void save(Morador morador);
	
	void update(Long id, Morador morador);
	
	void delete(Long id);
}
