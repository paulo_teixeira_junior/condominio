package com.gsoftware.condominio.Service;

import java.util.List;

import com.gsoftware.condominio.model.Unidade;

public interface UnidadeService {
	
	public List<Unidade> getAll();
	
	public Unidade getUnidade(Long id);
	
	void save(Unidade unidade);
	
	void update(Long id, Unidade unidade);
	
	void delete(Long id);
}
