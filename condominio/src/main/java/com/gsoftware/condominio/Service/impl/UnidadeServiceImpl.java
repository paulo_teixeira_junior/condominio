package com.gsoftware.condominio.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsoftware.condominio.Service.UnidadeService;
import com.gsoftware.condominio.model.Unidade;
import com.gsoftware.condominio.repository.UnidadeRepository;

@Service
public class UnidadeServiceImpl implements UnidadeService {

	@Autowired
	private UnidadeRepository unidadeRepository;
	@Override
	public List<Unidade> getAll() {
		return unidadeRepository.findAll();
	}

	@Override
	public Unidade getUnidade(Long id) {
		return unidadeRepository.findById(id).orElse(null);
	}

	@Override
	public void save(Unidade unidade) {
		unidadeRepository.save(unidade);
	}

	@Override
	public void update(Long id, Unidade unidade) {
		Unidade entity = unidadeRepository.findById(id).orElse(null);
		if(entity != null) {
			entity = unidade;
			entity.setId(id);
			unidadeRepository.save(entity);
		}
	}

	@Override
	public void delete(Long id) {
		unidadeRepository.deleteById(id);
	}

}
