package com.gsoftware.condominio.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsoftware.condominio.Service.MoradorService;
import com.gsoftware.condominio.model.Morador;
import com.gsoftware.condominio.repository.MoradorRepository;

@Service
public class MoradorServiceImpl implements MoradorService {

	@Autowired
	private MoradorRepository moradorRepository;
	
	@Override
	public List<Morador> getAll() {
		return moradorRepository.findAll();
	}

	@Override
	public Morador getMorador(Long id) {
		return moradorRepository.findById(id).orElse(null);
	}

	@Override
	public void save(Morador morador) {
		moradorRepository.save(morador);
	}

	@Override
	public void update(Long id, Morador morador) {
		Morador entity = moradorRepository.findById(id).orElse(null);
		if(entity != null) {
			entity = morador;
			entity.setId(id);
			moradorRepository.save(entity);
		}
	}

	@Override
	public void delete(Long id) {
		moradorRepository.deleteById(id);
	}

}
