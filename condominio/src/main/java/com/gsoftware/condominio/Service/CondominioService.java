package com.gsoftware.condominio.Service;

import java.util.List;

import com.gsoftware.condominio.model.Condominio;

public interface CondominioService {

	public List<Condominio> getAll();
	
	public Condominio getCondominio(Long id);
	
	void save(Condominio condominio);
	
	void update(Long id, Condominio condominio);
	
	void delete(Long id);
}
