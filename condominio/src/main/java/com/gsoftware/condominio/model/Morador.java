package com.gsoftware.condominio.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"nome", "sobreNome"})} )
public class Morador {

	public Morador() {
		
	}
	
	public Morador(MoradorPk moradorPk) {
		this.moradorPk = moradorPk;
	}
	
	public Morador(MoradorPk moradorPk, Unidade unidade) {
		this.moradorPk = moradorPk;
		this.unidade = unidade;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private MoradorPk moradorPk;
	
	@OneToOne(mappedBy = "morador", fetch = FetchType.LAZY)
	@JsonIgnore
	private Unidade unidade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MoradorPk getMoradorPk() {
		return moradorPk;
	}

	public void setMoradorPk(MoradorPk moradorPk) {
		this.moradorPk = moradorPk;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((moradorPk == null) ? 0 : moradorPk.hashCode());
		result = prime * result + ((unidade == null) ? 0 : unidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Morador other = (Morador) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (moradorPk == null) {
			if (other.moradorPk != null)
				return false;
		} else if (!moradorPk.equals(other.moradorPk))
			return false;
		if (unidade == null) {
			if (other.unidade != null)
				return false;
		} else if (!unidade.equals(other.unidade))
			return false;
		return true;
	}

}
