package com.gsoftware.condominio.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Unidade {

	public Unidade() {
		
	}
	
	public Unidade (Long numeroIdentificador) {
		this.numeroIdentificador = numeroIdentificador;
	}
	
	public Unidade (Long numeroIdentificador, Morador morador) {
		this(numeroIdentificador);
		this.morador = morador;
	}
	
	public Unidade (Long numeroIdentificador, Morador morador, Condominio condominio) {
		this(numeroIdentificador, morador);
		this.condominio = condominio;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true)
	private Long numeroIdentificador;
	
	@OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name = "moradorId", referencedColumnName = "id")
	private Morador morador;
	
	@ManyToOne
	private Condominio condominio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumeroIdentificador() {
		return numeroIdentificador;
	}

	public void setNumeroIdentificador(Long numeroIdentificador) {
		this.numeroIdentificador = numeroIdentificador;
	}

	public Morador getMorador() {
		return morador;
	}

	public void setMorador(Morador morador) {
		this.morador = morador;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	
}
