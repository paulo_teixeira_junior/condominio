package com.gsoftware.condominio;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.gsoftware.condominio.Service.CondominioService;
import com.gsoftware.condominio.model.Condominio;
import com.gsoftware.condominio.model.CondominioPK;
import com.gsoftware.condominio.model.Morador;
import com.gsoftware.condominio.model.MoradorPk;
import com.gsoftware.condominio.model.Unidade;

@Component
public class DataLoader implements CommandLineRunner {

	@Autowired
	private CondominioService condominioService;
	
	@Override
	public void run(String... args) throws Exception {
		try {
			CondominioPK condominioPk1 = new CondominioPK("Piacenza", "Avenida das américas");
			CondominioPK condominioPk2 = new CondominioPK("Piacenza", "Avenida");
			
			MoradorPk morador1Pk = new MoradorPk("Paulo", "Teixeira");
			MoradorPk morador2Pk = new MoradorPk("Paulo", "Junior");
			
			Morador morador1 = new Morador(morador1Pk);
			Morador morador2 = new Morador(morador2Pk);
			Condominio condominio1 = new Condominio(condominioPk1);
			Condominio condominio2 = new Condominio(condominioPk2);
			
			Unidade unidade1 = new Unidade(105L, morador1, condominio1);
			Unidade unidade2 = new Unidade(106L, morador2, condominio2) ;
			
			condominio1.setUnidades(Arrays.asList(unidade1));
			condominio2.setUnidades(Arrays.asList(unidade2));
			
			condominioService.save(condominio1);
			condominioService.save(condominio2);
		} catch (Exception e) {
			System.out.println(String.format("ERROR: %s.", e.getMessage()));
		}
		
		
	}

}
