package com.gsoftware.condominio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gsoftware.condominio.model.Condominio;

@Repository
public interface CondominioRepository extends JpaRepository<Condominio, Long> {

}
