package com.gsoftware.condominio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gsoftware.condominio.model.Morador;

@Repository
public interface MoradorRepository extends JpaRepository<Morador, Long> {

}
